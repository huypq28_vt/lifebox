package com.sourcey.materiallogindemo.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.widget.TextView;

import com.sourcey.materiallogindemo.SignupActivity;

public class Utils {
    private static volatile NProgressDialog progress;
    public static void showProgressDialog(final Activity activity) {
        if (activity == null)
            return;

        if (progress != null) {
            try {
                progress.cancel();
            } catch (Exception ex) {

            }
        }
        progress = new NProgressDialog(activity);
        progress.setTitle("");
        progress.setCancelable(true);
        progress.setOnCancelListener(null);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }
    public static void dismissProgressDialog() {
        try {
            if (progress != null)
                progress.dismiss();
        } catch (Exception e) {

        }
    }
        public static void dialogNotification(String title, String content, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        // Creates textview for centre title
        TextView myMsg = new TextView(context);
        myMsg.setText(title);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextSize(20);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
        builder.setCustomTitle(myMsg);
        builder.setMessage(content);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //Create custom message
        TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
        messageText.setTextColor(Color.BLACK);
        messageText.setGravity(Gravity.CENTER);
    }
}
