package com.sourcey.materiallogindemo.util;

public class ContantsAPI {
    public static final String GET_PROFILE = "https://lifebox.vn/sapi/profile?action=get";
    public static final String SIGNUP = "https://lifebox.vn/sapi/mobile?action=signup";
    public static final String LOGIN = "https://lifebox.vn/sapi/login?action=login";
    public static final String GET_INFO_STORAGE = "https://lifebox.vn/sapi/media?action=get-storage-space";
    public static final String GET_COUNT_PICTURE = "https://lifebox.vn/sapi/media/picture?action=count";
    public static final String DOWNLOAD_PICTURE = "https://lifebox.vn/sapi/media/picoftheday?action=get";
    public static final String GET_LIST_PICTURE = "https://lifebox.vn/sapi/media/picture?action=get";
    public static final String URL_UPLOAD_IMAGE = "https://lifebox.vn/sapi/upload/picture?action=save";   // bo qua
    public static final String URL_UPLOAD_IMAGE_DIRECT = "https://lifebox.vn//sapi/upload?action=save&type=picture";
    public static final String URL_UPLOAD_IMAGE_META_DATA = "https://lifebox.vn/sapi/upload/picture?action=save-metadata";
}
