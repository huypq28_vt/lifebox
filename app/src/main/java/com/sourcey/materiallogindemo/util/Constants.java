package com.sourcey.materiallogindemo.util;

import java.util.ArrayList;

public final class Constants {
	public static ArrayList<String> LIST_IMAGE_URL = new ArrayList<String>();

	private Constants() {

	}

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}
	
	public static class Extra {
		public static final String FRAGMENT_INDEX = "com.nostra13.example.universalimageloader.FRAGMENT_INDEX";
		public static final String IMAGE_POSITION = "com.nostra13.example.universalimageloader.IMAGE_POSITION";
	}
}
