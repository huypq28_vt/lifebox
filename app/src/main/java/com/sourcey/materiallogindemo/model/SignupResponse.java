package com.sourcey.materiallogindemo.model;

import lombok.Data;

/**
 * Data for JSON server format
 */
@Data
public class SignupResponse {
    private String data;
    private String responsetime;

    @Override
    public String toString() {
        return "EventLogResponse{" +
                "data='" + data + '\'' +
                ", responsetime='" + responsetime + '\'' +
                '}';
    }
}
