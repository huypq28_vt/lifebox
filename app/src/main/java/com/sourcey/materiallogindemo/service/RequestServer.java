package com.sourcey.materiallogindemo.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.google.gson.Gson;
import com.sourcey.materiallogindemo.callback.CallbackHomeNew;
import com.sourcey.materiallogindemo.callback.CallbackSignup;
import com.sourcey.materiallogindemo.callback.CallbackLogin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class RequestServer {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private Gson gson = new Gson();
    Context context;
    private CallbackSignup callbackSignup;
    private  CallbackLogin callbackLogin ;
    private CallbackHomeNew callbackHomeNew;

    public RequestServer(CallbackSignup callbackSignup, Context contextSignup) {
        context = contextSignup;
        this.callbackSignup = callbackSignup;
    }
    public RequestServer(CallbackLogin callbackLogin, Context contextLogin) {
        context = contextLogin;
        this.callbackLogin = callbackLogin;
    }
    public RequestServer (CallbackHomeNew callbackHomeNew, Context contextGetProfile){
        context = contextGetProfile;
        this.callbackHomeNew = callbackHomeNew;
    }
    public void postRequest(String postUrl, String postBody, Context context) throws IOException {

        context = context;
        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, postBody);

        okhttp3.Request request = new Request.Builder()
                .url(postUrl)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callbackSignup.onFailerSignup("Registration failed, recheck connection!");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    if (jsonData.length() <= 62){
                        if (Jobject.getJSONObject("data") != null){
                            callbackSignup.onSuccessSignup("Signup Success!");
                        }
                    } else if (Jobject.getJSONObject("error") != null){
                        callbackSignup.onSuccessSignup(Jobject.getJSONObject("error").get("message").toString() + " : "
                                + Jobject.getJSONObject("error").get("cause").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void requestLogin(String postUrl, String postBody, String username, String password, Context context) throws IOException {

        String credentials = username + ":" + password;
        final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        //save authention key
        saveKeyAuthention(context, basic);
        context = context;
        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, postBody);

        okhttp3.Request request = new Request.Builder()
                .url(postUrl)
                .header("Authorization", basic)
                .post(body)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callbackLogin.onFailerSignin("Login failed, recheck connection!");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    if (jsonData.length() <226){
                        String validationkey = Jobject.getJSONObject("data").getString("validationkey");

                        callbackLogin.onSuccessSignin("Login Success!", validationkey);
                    } else if (Jobject.getJSONObject("error") != null){
                        callbackLogin.onFailerSignin(Jobject.getJSONObject("error").get("message").toString() +"");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callbackLogin.onFailerSignin("Login failed!");
                }
            }
        });
    }


    public void requestGetProfile(String url, String keyAuthen, Context context) throws IOException {


        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", keyAuthen)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    String first_name = Jobject.getJSONObject("data").getJSONObject("user").getJSONObject("generic").getString("firstname")+"";
                    String last_name = Jobject.getJSONObject("data").getJSONObject("user").getJSONObject("generic").getString("lastname")+"";
                    callbackHomeNew.onSuccessGetProfile(last_name + " "+ first_name);
                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });
    }


    public void requestGetListImage(String url, String keyAuthen, String validationkey, Context context) throws IOException {


        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", keyAuthen)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    if (!"[]".equals(new JSONObject(jsonData).getJSONObject("data").getJSONArray("pictures").toString())){
                        JSONArray listPicture = Jobject.getJSONObject("data").getJSONArray("pictures");
                        int numPic = listPicture.length();
                        ArrayList<String> listUrl = new ArrayList<String>();
                        for (int i = 0; i< numPic; i++){
                            String url =  "https://lifebox.vn"+Jobject.getJSONObject("data").getJSONArray("pictures").getJSONObject(i).get("url").toString();
                            listUrl.add(url);
                        }
                        callbackHomeNew.onListImageSuccess(listUrl);
                    } else callbackHomeNew.onListImageFailer("");

                } catch (JSONException e) {
                    e.printStackTrace();

                }


            }
        });
    }


    public void requestInfoStorage(String url, String keyAuthen, Context context) throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", keyAuthen)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    String nolimit = Jobject.getJSONObject("data").getString("nolimit");

                    Float totalStorage = (Float.parseFloat(Jobject.getJSONObject("data").getString("quota")))/(1024*1024*1024) ;
                    Float freeStorage = (Float.parseFloat(Jobject.getJSONObject("data").getString("free")))/(1024*1024*1024) ;
                    Float usedStorage = (Float.parseFloat(Jobject.getJSONObject("data").getString("used")))/(1024*1024*1024) ;
                    String msg = "Total: " + totalStorage + "" +"Gb"+"\n" +
                            "Free: " + freeStorage+ " "+ "Gb"+ "\n" +
                            "Used: " + usedStorage + " " + "Gb"+ "\n" +
                            "Nolimit: " + nolimit;
                    callbackHomeNew.onSuccessInfoStorage(usedStorage);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });
    }
    public void requestGetNumberPicture(String url, String keyAuthen, Context context) throws IOException {

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", keyAuthen)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    String count = Jobject.getJSONObject("data").getString("count");
                    callbackHomeNew.onSuccessCountPicture(" Uploaded: " + count + " picture");

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });
    }

    public void saveKeyAuthention(Context context, String basic) {
                try {
                    SharedPreferences mPrefKey = context.getSharedPreferences("MyViettel", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = mPrefKey.edit();
                    editor.putString("Authorization", basic);
                    editor.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
    public void saveValidationKey(Context context, String validationkey) {
        try {
            SharedPreferences mPrefKey = context.getSharedPreferences("MyViettel", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = mPrefKey.edit();
            editor.putString("validationkey", validationkey);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
