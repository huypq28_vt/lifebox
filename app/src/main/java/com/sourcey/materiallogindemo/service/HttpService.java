package com.sourcey.materiallogindemo.service;

import android.content.Context;

import com.sourcey.materiallogindemo.callback.SendEventLogCallback;

/**
 * Generic interface for http service, when new http service is created, it should be extend this HttpService
 */
public interface HttpService {

    void sendEventLog(String postUrl, String postBody, final SendEventLogCallback callback);
}
