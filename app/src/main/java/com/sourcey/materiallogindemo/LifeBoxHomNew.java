package com.sourcey.materiallogindemo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourcey.materiallogindemo.activity.HomeActivity;
import com.sourcey.materiallogindemo.callback.CallbackHomeNew;
import com.sourcey.materiallogindemo.service.RequestServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

import static com.sourcey.materiallogindemo.util.Constants.LIST_IMAGE_URL;

import static com.sourcey.materiallogindemo.util.ContantsAPI.GET_COUNT_PICTURE;
import static com.sourcey.materiallogindemo.util.ContantsAPI.GET_INFO_STORAGE;
import static com.sourcey.materiallogindemo.util.ContantsAPI.GET_LIST_PICTURE;
import static com.sourcey.materiallogindemo.util.ContantsAPI.GET_PROFILE;

public class LifeBoxHomNew extends AppCompatActivity  implements CallbackHomeNew {
    SharedPreferences mPrefKey;
    private TextView txtName, txtStorageInfo, tittleStorage, tittleMedia, txtCountPicture ;
    private ImageView imgUploadPicture;
    private ImageView imgDownloadPicture;
    private String filePathImage;
    private RequestServer requestServer;
    private PieChartView pieChartView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_box_hom_new);
        initView();
        requestServer = new RequestServer(this, getApplicationContext());
        mPrefKey = getBaseContext().getSharedPreferences("MyViettel", Context.MODE_PRIVATE);
        String keyAuthen = mPrefKey.getString("Authorization","");
        String validationkey = mPrefKey.getString("validationkey","");
        try {
            requestServer.requestGetListImage(GET_LIST_PICTURE, keyAuthen, validationkey, getBaseContext());
            requestServer.requestGetProfile(GET_PROFILE, keyAuthen,  getBaseContext());
            requestServer.requestInfoStorage(GET_INFO_STORAGE, keyAuthen, getBaseContext());
            requestServer.requestGetNumberPicture(GET_COUNT_PICTURE, keyAuthen, getBaseContext());
        } catch (IOException e) {
            e.printStackTrace();
        }

        imgUploadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LifeBoxHomNew.this, UploadActivity.class);
                startActivity(intent);
            }
        });
        imgDownloadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LifeBoxHomNew.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void initView() {
        txtName = findViewById(R.id.txtName);
        txtStorageInfo = findViewById(R.id.storageInfo);
        tittleStorage = findViewById(R.id.tittleStorage);
        tittleMedia = findViewById(R.id.tittleMedia);
        txtCountPicture = findViewById(R.id.txtCountPicture);
        imgUploadPicture = findViewById(R.id.imgUploadPicture);
        imgDownloadPicture = findViewById(R.id.imgDownloadPicture);
        pieChartView = findViewById(R.id.chartStorage);
    }

    // get profile
    @Override
    public void onSuccessGetProfile(final String msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                txtName.setText("Hello " + msg);
            }
        });

    }

    @Override
    public void onFailerGetProfile(String msg) {

    }

    // info storage
    @Override
    public void onSuccessInfoStorage(final Float msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                List<SliceValue> pieData = new ArrayList<>();
                pieData.add(new SliceValue(msg, Color.RED).setLabel("Q1: $10"));
                pieData.add(new SliceValue(5 -msg, Color.YELLOW).setLabel("Q2: $4"));
//                txtStorageInfo.setText(msg);
                tittleStorage.setVisibility(View.VISIBLE);
                PieChartData pieChartData = new PieChartData(pieData);
                pieChartData.setHasCenterCircle(true).setCenterText1(Math.ceil(msg * 10) / 10 + "/5GB" + "\nĐã sử dụng").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
                pieChartView.setPieChartData(pieChartData);
            }
        });
    }

    @Override
    public void onFailerInfoStorage(String msg) {

    }


    // get number picture
    @Override
    public void onSuccessCountPicture(final String msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                txtCountPicture.setText(msg);
                tittleMedia.setVisibility(View.VISIBLE);
                imgUploadPicture.setVisibility(View.VISIBLE);
                imgDownloadPicture.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onFailerCountPicture(String msg) {

    }

    // downloadn image
    @Override
    public void onListImageSuccess(ArrayList<String> listUrl) {
        LIST_IMAGE_URL = listUrl;
    }

    @Override
    public void onListImageFailer(String msg) {

    }

}
