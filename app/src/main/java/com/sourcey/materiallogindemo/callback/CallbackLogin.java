package com.sourcey.materiallogindemo.callback;

public interface CallbackLogin {
    void onSuccessSignin(String msg, String validationkey);
    void onFailerSignin(String msg);
}
