package com.sourcey.materiallogindemo.callback;

public interface CallbackSignup {
    void onSuccessSignup(String msg);
    void onFailerSignup(String msg);
}
