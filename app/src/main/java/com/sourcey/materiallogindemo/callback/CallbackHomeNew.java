package com.sourcey.materiallogindemo.callback;

import java.util.ArrayList;

public interface CallbackHomeNew {
    // get profile
    void onSuccessGetProfile(String msg);
    void onFailerGetProfile(String msg);

    // get info storage
    void onSuccessInfoStorage(Float msg);
    void onFailerInfoStorage(String msg);

    // get count picture
    void onSuccessCountPicture(String msg);
    void onFailerCountPicture(String msg);

    // download image
    void onListImageSuccess(ArrayList<String> listUrl);
    void onListImageFailer(String msg);
}
