package com.sourcey.materiallogindemo.callback;


import com.sourcey.materiallogindemo.model.SignupResponse;

public interface SendEventLogCallback {
    /**
     * Callback in case sending data to server successful
     *
     * @param signupResponse The response from server
     */
    void onSignupSuccessful(SignupResponse signupResponse);

    /**
     * Callback in case sending data to server failed
     *
     * @param message The message from server
     */
    void onSignupFailed(String message);
}
