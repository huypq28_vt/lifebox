package com.sourcey.materiallogindemo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.parser.JSONParser;
import com.sourcey.materiallogindemo.permission.PermissionsActivity;
import com.sourcey.materiallogindemo.permission.PermissionsChecker;
import com.sourcey.materiallogindemo.util.InternetConnection;
import com.sourcey.materiallogindemo.util.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class UploadActivity extends AppCompatActivity {
    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    Context mContext;
    private ImageView imageView;
    private TextView textView;
    private String imagePath;
    private PermissionsChecker checker;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    SharedPreferences mPrefKey;;
    String keyAuthen;
    String validationkey;
    String idUpload;
    String contentType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        mContext = getApplicationContext();
        checker = new PermissionsChecker(this);
        initView();
        mPrefKey = getBaseContext().getSharedPreferences("MyViettel", Context.MODE_PRIVATE);
        keyAuthen = mPrefKey.getString("Authorization","");
        validationkey = mPrefKey.getString("validationkey","");
        if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
            startPermissionsActivity(PERMISSIONS_READ_STORAGE);
        } else {
            showImagePopup();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(imagePath)) {
                    if (InternetConnection.checkConnection(mContext)) {
                        new AsyncTask<Void, Integer, String>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                Utils.showProgressDialog(UploadActivity.this);
                            }

                            @Override
                            protected String doInBackground(Void... params) {

                                String jsonResult = JSONParser.uploadDirectImage(imagePath, keyAuthen, validationkey );
                                if(jsonResult == null){
                                    return "";
                                }
                                return jsonResult;
                            }

                            @Override
                            protected void onPostExecute(String jsonResult) {
                                super.onPostExecute(jsonResult);
                                Utils.dismissProgressDialog();
                                Toast.makeText(getApplicationContext(), R.string.string_save_success, Toast.LENGTH_LONG).show();
                                String jsonUpload  = jsonResult;
//                                if (jsonUpload != ""){
//                                    idUpload = jsonUpload.substring(53,62);
//                                    // content type
//                                    String[] part = jsonUpload.split("huypq28");
//                                    contentType = part[1];
//                                }
                                imagePath = "";
                                textView.setVisibility(View.VISIBLE);
                                imageView.setVisibility(View.INVISIBLE);
                                fab.setVisibility(View.INVISIBLE);
                            }
                        }.execute();
                    } else {
                        Snackbar.make(findViewById(R.id.parentView), R.string.string_internet_connection_warning, Snackbar.LENGTH_INDEFINITE).show();
                    }
                } else {
                    Snackbar.make(findViewById(R.id.parentView), R.string.string_message_to_attach_file, Snackbar.LENGTH_INDEFINITE).show();
                }
            }
        });


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONParser.commitImage(keyAuthen, validationkey, idUpload, contentType);
            }
        });
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        fab =  findViewById(R.id.fab);
        textView = findViewById(R.id.textView);
        imageView = findViewById(R.id.imageView);
    }

    /**
     * Showing Image Picker
     */
    private void showImagePopup() {
        // File System.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of file system options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, getString(R.string.string_choose_image));
        startActivityForResult(chooserIntent, 1010);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1010) {
            if (data == null) {
                Snackbar.make(findViewById(R.id.parentView), R.string.string_unable_to_pick_image, Snackbar.LENGTH_INDEFINITE).show();
                return;
            }
            Uri selectedImageUri = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);

                Picasso.with(mContext).load(new File(imagePath))
                        .into(imageView);
                cursor.close();

            } else {
                Snackbar.make(findViewById(R.id.parentView), R.string.string_unable_to_load_image, Snackbar.LENGTH_LONG).setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showImagePopup();
                    }
                }).show();
            }

            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        }
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(this, 0, permission);
    }
}
