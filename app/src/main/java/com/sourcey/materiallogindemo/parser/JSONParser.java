package com.sourcey.materiallogindemo.parser;

import android.util.Log;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.sourcey.materiallogindemo.service.RequestServer.JSON;
import static com.sourcey.materiallogindemo.util.ContantsAPI.URL_UPLOAD_IMAGE;
import static com.sourcey.materiallogindemo.util.ContantsAPI.URL_UPLOAD_IMAGE_DIRECT;
import static com.sourcey.materiallogindemo.util.ContantsAPI.URL_UPLOAD_IMAGE_META_DATA;

public class JSONParser {

    public static String uploadImage(String sourceImageFile, String authenKey, String validationkey) {

        try {
            String url = URL_UPLOAD_IMAGE_META_DATA+"&"+"validationkey"+"="+validationkey;
            File sourceFile = new File(sourceImageFile);
            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");
            String filename = sourceImageFile.substring(sourceImageFile.lastIndexOf("/")+1);
            String creationdate = "";
            String modificationdate = "";
            String contentType = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentType().type();
            String size = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentLength()+"";
            String postBody =    "{"+  " \"data\": { \n" +
                    "    \"name\": " + "\"" + filename + "\" "+ ",\n" +
                    "    \"creationdate\": " + "\""+ creationdate + "\" "+ ",\n" +
                    "    \"modificationdate\": " +"\""+ modificationdate + "\""+",\n" +
                    "    \"contenttype\": " +"\""+ contentType + "\""+",\n" +
                    "    \"size\": " +"\""+ size + "\""+",\n" +
                    "    \"clientproperties\": \"[]\",\n" +
                    "} \n" +
                    "}";
            RequestBody body = RequestBody.create(JSON, postBody);

            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", authenKey)
                    .addHeader("Content-Type", "multipart/form-data")
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String res = response.body().string();
            return res+"huypq28"+contentType;

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("TAG", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("TAG", "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }


    public static String commitImage( String authenKey, String validationkey, String idUpload, String contentType) {

        try {
            String url = URL_UPLOAD_IMAGE+"&"+"validationkey"+"="+validationkey;

            String postBody = "";

            RequestBody body = RequestBody.create(JSON, postBody);

            Request request = new Request.Builder()
                    .url(url)
                    .header("Authorization", authenKey)
                    .addHeader("x-funambol-id", idUpload)
                    .addHeader("Content-Type", contentType)
                    .post(body)
                    .build();

            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String res = response.body().string();
//            JSONObject jsonObject = new JSONObject(res);
            return res;

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("TAG", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("TAG", "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }


    public static String uploadDirectImage(String sourceImageFile, String authenKey, String validationkey) {

        try {
            String url = URL_UPLOAD_IMAGE_DIRECT;
            File sourceFile = new File(sourceImageFile);
            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");
            String filename = sourceImageFile.substring(sourceImageFile.lastIndexOf("/")+1);
            String creationdate = "";
            String modificationdate = "";
            String contentType = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentType().type();
            String size = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentLength()+"";
            String postBody = "{"+  " \"data\": { \n" +
                                    "    \"data\":  { \n" +
                                        "    \"name\": " + "\"" + filename + "\" "+ ",\n" +
                                        "    \"creationdate\": " + "\""+ creationdate + "\" "+ ",\n" +
                                        "    \"modificationdate\": " +"\""+ modificationdate + "\""+",\n" +
                                        "    \"contenttype\": " +"\""+ contentType + "\""+",\n" +
                                        "    \"size\": " +"\""+ size + "\""+",\n" +
                                        "    \"clientproperties\": \"[]\",\n" +
                                    "} \n" +
                                    "} \n" +
                                    "}";
            RequestBody body = RequestBody.create(JSON, postBody);
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"data\""), body)
//                    .addFormDataPart("data", filename, RequestBody.create(MEDIA_TYPE_PNG, sourceFile))
//                    .addFormDataPart("name", filename)
//                    .addFormDataPart("contenttype", RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentType().type())
//                    .addFormDataPart("size", String.valueOf(RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentLength()))
//                    .addFormDataPart("folderid", "1")
                    .build();
//            String creationdate = "";
//            String modificationdate = "";
//            String contentType = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentType().type();
//            String size = RequestBody.create(MEDIA_TYPE_PNG, sourceFile).contentLength()+"";
//            String postBody =    " \"Content-Disposition: form-data; name=\"data\"\"\n" +
//                                    "{"+  " \"data\": { \n" +
//                                    "    \"data\":  { \n" +
//                                    "    \"name\": " + "\"" + filename + "\" "+ ",\n" +
//                                    "    \"creationdate\": " + "\""+ creationdate + "\" "+ ",\n" +
//                                    "    \"modificationdate\": " +"\""+ modificationdate + "\""+",\n" +
//                                    "    \"contenttype\": " +"\""+ contentType + "\""+",\n" +
//                                    "    \"size\": " +"\""+ size + "\""+",\n" +
//                                    "    \"clientproperties\": \"[]\",\n" +
//                                    "} \n" +
//                                    "} \n" +
//                                    "}";
//            RequestBody body = RequestBody.create(JSON, postBody);
            Request request = new Request.Builder()
                    .url(url)
                    .header("Authorization", authenKey)
                    .addHeader("Content-Type", "multipart/form-data;boundary=" + "*****")
                    .post(requestBody)
                    .build();
            OkHttpClient client = new OkHttpClient();
            Response response = client.newCall(request).execute();
            String res = response.body().string();
            return res;

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("TAG", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("TAG", "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }

}
