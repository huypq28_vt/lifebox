package com.sourcey.materiallogindemo.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.L;
import com.sourcey.materiallogindemo.R;
import com.sourcey.materiallogindemo.fragment.ImageGalleryFragment;
import com.sourcey.materiallogindemo.fragment.ImageGridFragment;
import com.sourcey.materiallogindemo.fragment.ImageListFragment;
import com.sourcey.materiallogindemo.fragment.ImagePagerFragment;
import com.sourcey.materiallogindemo.service.RequestServer;
import com.sourcey.materiallogindemo.util.Constants;
import com.sourcey.materiallogindemo.util.Utils;

import static com.sourcey.materiallogindemo.util.Constants.LIST_IMAGE_URL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static com.sourcey.materiallogindemo.util.ContantsAPI.GET_LIST_PICTURE;

public class HomeActivity extends Activity {
	private RequestServer requestServer;
	private SharedPreferences mPrefKey;
	private ImageView imgPicture;
	private String urlImage = "";
	private static final int CONTENT_VIEW_ID = 10101010;
	private static final String TEST_FILE_NAME = "Universal Image Loader @#&=+-_.,!()~'%20.png";
	private TextView tittle;
	private Button listView, gridView, viewPager, gallery, listgridView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_home);
		initView();
//		Utils.showProgressDialog(HomeActivity.this);
//		requestServer = new RequestServer(this, getApplicationContext());
//		mPrefKey = getBaseContext().getSharedPreferences("MyViettel", Context.MODE_PRIVATE);
//		String keyAuthen = mPrefKey.getString("Authorization","");
//		String validationkey = mPrefKey.getString("validationkey","");
//		try {
//			requestServer.requestGetListImage(GET_LIST_PICTURE, keyAuthen, validationkey, getBaseContext());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		File testImageOnSdCard = new File("/mnt/sdcard", TEST_FILE_NAME);
		if (!testImageOnSdCard.exists()) {
			copyTestImageToSdCard(testImageOnSdCard);
		}
		Intent intent = new Intent(HomeActivity.this, SimpleImageActivity.class);
		intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImageGridFragment.INDEX);
		startActivity(intent);
	}

	private void initView() {
		tittle = findViewById(R.id.tittle);
		listView = findViewById(R.id.listView);
		gridView = findViewById(R.id.gridView);
		viewPager = findViewById(R.id.viewPager);
		gallery = findViewById(R.id.gallery);
		listgridView = findViewById(R.id.listgridView);
	}

	public void onImageListClick(View view) {
		Intent intent = new Intent(this, SimpleImageActivity.class);
		intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImageListFragment.INDEX);
		startActivity(intent);
	}

	public void onImageGridClick(View view) {
		Intent intent = new Intent(this, SimpleImageActivity.class);
		intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImageGridFragment.INDEX);
		startActivity(intent);
	}

	public void onImagePagerClick(View view) {
		Intent intent = new Intent(this, SimpleImageActivity.class);
		intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImagePagerFragment.INDEX);
		startActivity(intent);
	}

	public void onImageGalleryClick(View view) {
		Intent intent = new Intent(this, SimpleImageActivity.class);
		intent.putExtra(Constants.Extra.FRAGMENT_INDEX, ImageGalleryFragment.INDEX);
		startActivity(intent);
	}

	public void onFragmentsClick(View view) {
		Intent intent = new Intent(this, ComplexImageActivity.class);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		ImageLoader.getInstance().stop();
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.item_clear_memory_cache:
				ImageLoader.getInstance().clearMemoryCache();
				return true;
			case R.id.item_clear_disc_cache:
				ImageLoader.getInstance().clearDiskCache();
				return true;
			default:
				return false;
		}
	}

	private void copyTestImageToSdCard(final File testImageOnSdCard) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					InputStream is = getAssets().open(TEST_FILE_NAME);
					FileOutputStream fos = new FileOutputStream(testImageOnSdCard);
					byte[] buffer = new byte[8192];
					int read;
					try {
						while ((read = is.read(buffer)) != -1) {
							fos.write(buffer, 0, read);
						}
					} finally {
						fos.flush();
						fos.close();
						is.close();
					}
				} catch (IOException e) {
					L.w("Can't copy test image onto SD card");
				}
			}
		}).start();
	}
}