package com.sourcey.materiallogindemo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sourcey.materiallogindemo.callback.CallbackSignup;
import com.sourcey.materiallogindemo.service.HttpService;
import com.sourcey.materiallogindemo.service.RequestServer;
import com.sourcey.materiallogindemo.util.Utils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity implements  CallbackSignup {
    private static final String TAG = "SignupActivity";

    @BindView(R.id.input_name) EditText _nameText;
    @BindView(R.id.input_address) EditText _addressText;
    @BindView(R.id.input_mobile) EditText _mobileText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;

    private HttpService httpService;
    private RequestServer requestServer;
    public String postUrl = "https://lifebox.vn/sapi/mobile?action=signup";

//    public String postBody =    "{"+  " \"data\": { \n" +
//                                "    \"user\":  { \n" +
//                                "    \"phonenumber\": \"+84348114968\",\n" +
//                                "    \"password\": \"123456\",\n" +
//                                "    \"firstname\": \"Huy\",\n" +
//                                "    \"lastname\": \"Quang\",\n" +
//                                "    \"platform\": \"android\",\n" +
//                                "    \"model\": \"8800\",\n" +
//                                "    \"carrier\": \"Viettel\",\n" +
//                                "    \"preferredcommunicationchannel\": \"sms\",\n" +
//                                "    \"acceptedtermsandconditions\": \"true\"\n" +
//                                "} \n" +
//                                "} \n" +
//                                "}";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        requestServer = new RequestServer(this, getApplicationContext());
        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }
    public void signup() {
        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(true);

        Utils.showProgressDialog(SignupActivity.this);
        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String mobile = "+" + _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        String postBody =    "{"+  " \"data\": { \n" +
                "    \"user\":  { \n" +
                "    \"phonenumber\": " + "\"" + mobile + "\" "+ ",\n" +
                "    \"password\": " + "\""+ password + "\" "+ ",\n" +
                "    \"firstname\": " +"\""+ name + "\""+",\n" +
                "    \"lastname\": \" \",\n" +
                "    \"platform\": \"android\",\n" +
                "    \"model\": \"8800\",\n" +
                "    \"carrier\": \"Viettel\",\n" +
                "    \"preferredcommunicationchannel\": \"sms\",\n" +
                "    \"acceptedtermsandconditions\": \"true\"\n" +
                "} \n" +
                "} \n" +
                "}";

        // TODO: Implement your own signup logic here.
        // call api
        try {
            requestServer.postRequest(postUrl, postBody, getBaseContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
//                        onSignupSuccess();
                        Utils.dismissProgressDialog();
                    }
                }, 5000);
    }

    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String address = _addressText.getText().toString();
        String mobile = _mobileText.getText().toString();
        String password = _passwordText.getText().toString();
        String reEnterPassword = _reEnterPasswordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (address.isEmpty()) {
            _addressText.setError("Enter Valid Address");
            valid = false;
        } else {
            _addressText.setError(null);
        }

        if (mobile.isEmpty() || mobile.length() < 11) {
            _mobileText.setError("Enter Valid Mobile Number");
            valid = false;
        } else {
            _mobileText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (reEnterPassword.isEmpty() || reEnterPassword.length() < 4 || reEnterPassword.length() > 10 || !(reEnterPassword.equals(password))) {
            _reEnterPasswordText.setError("Password do not match");
            valid = false;
        } else {
            _reEnterPasswordText.setError(null);
        }

        return valid;
    }


    @Override
    public void onSuccessSignup(final String msg) {
//        this.runOnUiThread(new Runnable() {
//            public void run() {
//                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_SHORT).show();
//            }
//        });
        if ("Signup Success!".equals(msg)){
            this.runOnUiThread(new Runnable() {
            public void run() {
                dialogNotification("Signup Success", "Please click on the link in the message from 1098 to complete the account registration.");
            }
        });

        } else {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    dialogNotification("Registration failed", msg);
                }
            });

        }
    }

    @Override
    public void onFailerSignup(final String msg) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }
    public void dialogNotification(String title, String content){
        new AlertDialog.Builder(SignupActivity.this)
                .setTitle(title)
                .setMessage(content)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }


}